/*
	Nome dos Integrantes:
		Nome:Andr� Hallwas Ribeiro Alves.
		Nome:Armando Marioto Neto
		Nome:Lu�s Henrique Picinin Jandre
		
	Faculdade: UNOESTE ; 
	
	Data de Inicio: 12/11/15 20:28;
	Data de Termino: 19/11/15 01:47;
	Data de Entrega: 25/11/15 23:00;
	
	Descri��o: Trabalho de Geometria Anal�tica do segundo bimestre do segundo semestre.
*/
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////BIBILHOTECAS////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#include<stdio.h>
#include<locale.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>
#include<windows.h>
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////CONSTANTES//////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#define TV 3
#define TN 4

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////DECLARA��O DE MODULOS///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
int func_LDouLIplan(float N1[TN], float N2[TN]);
int func_pontonaEqGeral(float a[3], float b[TN]);
void func_leplan(float N[TN]);
void func_levet(float v[3], float pv[3]);
void func_levetoreponto(float a[3], float pa[3], float b[3], float pb[3]);
void func_sair(void);
void func_prosseguir(void);
void func_vetdiretor(float p[TV],float p1[TV],float vd[TV]);
void func_vetnormal(float v[TV],float v1[TV],float vn[TV]);
void func_digitaponto(float v[TV]);
void func_digitavetor(float v[TV]);
void func_pescalar(float v[TV],float v1[TV]);
void func_reqgeraldoplano(float v[TN]);
void func_Vetorial(float a[3], float b[3], float c[3]);
float func_ProdMisto(float a[3], float b[3], float ab[3]);
float func_lamb(float pb, float a, float pa, int &ver);
float func_ProdEScalar(float a[3], float b[TN]);
float divisao(float a, float b);
float func_escalar(float v[TV],float v1[TV]);
float func_modulo(float v[TV]);
float func_det(float m[TV][TV]);
float func_vetdir(float vn[TV]);
float func_vetnor(float vn[TV]);
int func_dl(float v[3], float v1[3]);
int func_pontonareta(float pb[3], float a[3], float pa[3]);
int func_dl(float v[TV],float v1[TV]);
void main_posrelativa_rp(float v[3], float pv[3], float N[TN]);
void main_posrelativa_pp(float N1[TN], float N2[TN]);
void main_posrelativa_rr(float a[3], float pa[3], float b[3], float pb[3]);
void main_aut_distancia_pop(float p[TV], float p1[TV],float vn[TV]);
void main_aut_distancia_por(float p[TV],float p1[TV],float vd1[TV]);
void main_distancia_pop(void);
void main_distancia_pp(void);
void main_distancia_por(void);
void main_distancia_rp(void);
void main_distancia_rr(float v1[3], float p1[3], float v2[3], float p2[3]);
void main_angulo_rr(void);
void main_angulo_rp(void);
void main_angulo_pp(void);
int main_aut_posrelativa_pp(float N1[TN], float N2[TN]);
int main_aut_posrelativa_rp(float v[3], float pv[3], float N[TN]);
int main_aut_posrelativa_rr(float a[3], float pa[3], float b[3], float pb[3]);
char menu_main(void);
char menu_rp(void);
void executa_geral(void);
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////FUN��ES NECESS�RIAS/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
void func_reqgeraldoplano(float v[TN]){
	int i=0;
	char opcao=75,opcao1=75;
	v[i]=v[i+1]=v[i+2]=v[i+3]=0;
	while(opcao1!='1'){
		i=0;
		do{
		if(opcao1=='2'){
			system("cls");
		}
		else
		  if(i>0){
			system("cls");
		  }	
		  else
		    printf("\n");
		printf("\nDigite os Componentes da Equa��o Geral do Plano \nNo Seguinte Formato: x + y + z + & e Pressione [[Enter]] \n\n");
		printf("\nDigite os Componentes um por um e Pressione [enter]\n");
		printf("\nDigite o %d� Componente\n",i+1);
		
		if(i==0){
			printf("Plano= ");
		}
		if(i==1){
			printf("(%0.1fx) +  ",v[0]);
		}
		if(i==2){
			printf("(%0.1fx) + (%0.1fy) + ",v[0],v[1]);
		}
		if(i==3){
			printf("(%0.1fx) + (%0.1fy) + (%0.1fz) + ",v[0],v[1],v[2]);
		}
		
				scanf("%f",&v[i]);
		i++;
		}while(i<4);
		opcao1=75;
		i=0;
		while(opcao1!='1'&&opcao1!='2'){
		system("cls");
		printf("\n\n\t\tPlano= (%0.1fx) + (%0.1fy) + (%0.1fz) + (%0.1f) \n\n",v[0],v[1],v[2],v[3]);
		printf("\n\t\t\t Escolha a Op��o Desejada: \n");
		printf("\n\n--Pressione o bot�o [1] Para Prosseguir; ");
		printf("\n--Pressione o bot�o [2] Para Alterar os Componentes;\n\n");
		if(opcao1!='1'&&opcao1!='2'&&i>0){
			printf("\n\tOp��o inv�lida,Digite Novamente a Op��o Desejada:\n");
		}
		fflush(stdin);
		opcao1=getche();
		i++;	
		}
	}
	system("cls");
	printf("\n\nPlano= (%0.1fx) + (%0.1fy) + (%0.1fz) + (%0.1f) \n\n",v[0],v[1],v[2],v[3]);
}
void func_levetoreponto(float a[3], float pa[3], float b[3], float pb[3])
{
	int i;
	system("cls");
	printf("\tInsira as Coordenadas do 1�Vetor\n\n");
		func_digitavetor(a);
		system("cls");
	printf("\tInsira as Coordenadas do Ponto\n\n");
		func_digitaponto(pa);
		system("cls");
	printf("\n\tX=(%0.1f , %0.1f , %0.1f) Vetor1=(%0.1f , %0.1f , %0.1f)\n\n",pa[0],pa[1],pa[2],a[0],a[1],a[2]);
	printf("\tInsira as Coordenadas do 2�Vetor\n\n");
		func_digitavetor(b);
		system("cls");
	printf("\tInsira as Coordenadas do Ponto\n\n");
		func_digitaponto(pb);
		system("cls");
	printf("\n\tX2=(%0.1f , %0.1f , %0.1f) Vetor2=(%0.1f , %0.1f , %0.1f)\n\n",pb[0],pb[1],pb[2],b[0],b[1],b[2]);
	func_prosseguir();
	system("cls");
	printf("\n\t X=(%0.1f , %0.1f , %0.1f) Vetor1=(%0.1f , %0.1f , %0.1f)\n",pa[0],pa[1],pa[2],a[0],a[1],a[2]);	
	printf("\n\t\t\t\t e \n");
	printf("\n\t X2=(%0.1f , %0.1f , %0.1f) Vetor2=(%0.1f , %0.1f , %0.1f)\n\n",pb[0],pb[1],pb[2],b[0],b[1],b[2]);
}
float func_lamb(float pb, float a, float pa, int &ver)
{
	float lambida;
	if(a==0)
	{
		if(pa==pb)
			lambida=pa;
		else
			ver=1;
	}
	else
		lambida=(pb-pa)/a;
	return lambida;
}
int func_pontonareta(float pb[3], float a[3], float pa[3])
{
	float lambx, lamby, lambz;
	int ver=0;
	lambx=func_lamb(pb[0], a[0], pa[0], ver);
	lamby=func_lamb(pb[1], a[1], pa[1], ver);
	lambz=func_lamb(pb[2], a[2], pa[2], ver);
	if(lambx==lamby&&lambx==lambz&&ver==0)
		return 1;
	else
		return 0;
}
void func_Vetorial(float a[3], float b[3], float c[3])
{
	float  i=0, j=0, k=0;
	i+= a[1]*b[2];
	j+= a[2]*b[0];
	k+= a[0]*b[1];
	i+= (a[0]*b[2])*(-1);
	j+= (a[2]*b[1])*(-1);
	k+= (a[1]*b[0])*(-1);
	c[0]= i;
	c[1]= j;
	c[2]= k;
}
int func_dl(float v[3], float v1[3])
{
	float v3[3], aux;
	int veri=0;
	if(v1[0]!=0&&v[0]!=0)
	{
		aux=v[0]/v1[0];
		veri=1;
	}
	else if(v1[1]!=0&&v[1]!=0)
	{
		aux=v[1]/v1[1];
		veri=1;
	}
		
	else if(v1[2]!=0&&v[2]!=0)
	{
		aux=v[2]/v1[2];
		veri=1;
	}
	v3[0]=aux*v1[0];
	v3[1]=aux*v1[1];
	v3[2]=aux*v1[2];
	if( v3[0]==v[0] && v3[1]==v[1] && v3[2]==v[2] && veri==1)
	{
		//printf("\n\n� LD\n\n");
		return 1;
	}
	else
	{
		//printf("\n\n� LI\n\n");
		return 0;
	}
}
float func_ProdMisto(float a[3], float b[3], float ab[3])
{
	float mis=0;
	mis+=(a[0]*b[1]*ab[2]);
	mis+=(a[1]*b[2]*ab[0]);
	mis+=(a[2]*b[0]*ab[1]);
	mis+=((ab[0]*b[1]*a[2])*(-1));
	mis+=((ab[1]*b[2]*a[0])*(-1));
	mis+=((ab[2]*b[0]*a[1])*(-1));
	return mis;
}
void func_levet(float v[3], float pv[3])
{
	int i;
	system("cls");
	printf("\tInsira os Valores do Vetor Diretor da Reta: \n\n");
		func_digitavetor(v);
	system("cls");
	printf("\tInsira os Valores do Ponto da Reta: \n\n");
		func_digitaponto(pv);
	system("cls");
	printf("\n\tX=(%0.1f , %0.1f , %0.1f) Vetor1=(%0.1f , %0.1f , %0.1f)\n\n",pv[0],pv[1],pv[2],v[0],v[1],v[2]);
}
float func_ProdEScalar(float a[3], float b[TN])
{
	float pr;
	pr=(a[0]*b[0]+ a[1]*b[1]+ a[2]*b[2]);
	return pr;
}
int func_pontonaEqGeral(float a[3], float b[TN])
{
	int ver;
	ver= a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+b[TN];
	if(ver==0)
		return 1;
	else
		return 0;
}
float divisao(float a, float b)
{
	float r;
	r=a/b;
	return r;
}
int func_LDouLIplan(float N1[TN], float N2[TN])
{
	float v3[TN];
	v3[0]=divisao(N1[0], N2[0]);
	v3[1]=divisao(N1[1], N2[1]);
	v3[2]=divisao(N1[2], N2[2]);
	v3[3]=divisao(N1[3], N2[3]);
	if(v3[0]==v3[1]&&v3[1]==v3[2]&&v3[2]==v3[3])
		return 45;
	else if(v3[0]==v3[1]&&v3[1]==v3[2]&&v3[2]!=v3[3])
		return 47;
	else if(v3[0]!=v3[1] || v3[1]!=v3[2] || v3[2]!=v3[1])
		return 49;
}
void func_sair(void){
	printf("\n\n Pressione [Enter] PARA SAIR\n\n");
			system("pause");
}

void func_prosseguir(void){
	printf("\n\n Pressione [Enter] PARA Prosseguir\n\n");
			system("pause");
}

float func_modulo(float v[TV]){
	float m;
	m=sqrt(pow(v[0],2)+pow(v[1],2)+pow(v[2],2));
	return m;
}
float func_det(float m[TV][TV]){
	float det;
	det=+((m[0][0]*m[1][1]*m[2][2])+(m[0][1]*m[1][2]*m[2][0])+(m[0][2]*m[1][0]*m[2][1]))-((m[2][0]*m[1][1]*m[0][2])+(m[2][1]*m[1][2]*m[0][0])+(m[2][2]*m[1][0]*m[0][1]));
	printf("\n O Determinante � :%0.1f \n",det);
	return det;
}
void func_pescalar(float v[TV],float v1[TV]){
	if((v[0]*v1[0]+v[1]*v1[1]+v[2]*v1[2])==0){
		printf("\n S�o Ortogonais!!\n");
	}    
	else
		printf("\n N�o s�o Ortogonais!!\n");
}

void func_digitaponto(float v[TV]){
	int i=0;
	char opcao=75,opcao1=75;
	v[i]=v[i+1]=v[i+2]=0;
	while(opcao1!='1'){
		i=0;
		do{
		if(opcao1=='2'){
			system("cls");
		}
		else
		  if(i>0){
			system("cls");
		  }	
		  else
		    printf("\n");
		printf("\nDigite os Componentes no seguinte formato: (x , y , z) e pressione [Enter] \n\n");
		printf("\nDigite os Componentes um por um e Pressione [enter]\n");
		printf("\nDigite o %d� Componente\n",i+1);
		
		if(i==0){
			printf("(");
		}
		if(i==1){
			printf("(%0.1f ,",v[0]);
		}
		if(i==2){
			printf("(%0.1f , %0.1f ,",v[0],v[1]);
		}
		
				scanf("%f",&v[i]);
		i++;
		}while(i<TV);
		opcao1=75;
		i=0;
		while(opcao1!='1'&&opcao1!='2'){
		system("cls");
		printf("\n\n\t\t\tComponentes= (%0.1f , %0.1f ,%0.1f)\n\n",v[0],v[1],v[2]);
		printf("\n\t\t\t Escolha a Op��o Desejada: \n");
		printf("\n\n--Pressione o bot�o [1] Para Prosseguir; ");
		printf("\n--Pressione o bot�o [2] Para Alterar os Componentes;\n\n");
		if(opcao1!='1'&&opcao1!='2'&&i>0){
			printf("\n\tOp��o inv�lida,Digite Novamente a Op��o Desejada:\n");
		}
		fflush(stdin);
		opcao1=getche();
		i++;	
		}
	}
	system("cls");
	printf("\n\nComponentes= (%0.1f , %0.1f ,%0.1f)\n\n",v[0],v[1],v[2]);
}

void func_digitavetor(float v[TV]){
	int i=0,z=0;
	char opcao=75,opcao1=75;
	v[i]=v[i+1]=v[i+2]=0;
	while(opcao1!='1'){
		i=0;
		do{
		if(opcao1=='2'){
			system("cls");
		}
		else
		  if(i>0){
			system("cls");
		  }	
		  else
		    printf("\n");
		printf("\nDigite os Componentes no seguinte formato: (x , y , z) e pressione [Enter] \n\n");
		printf("\nDigite os Componentes um por um e Pressione [enter]\n");
		printf("\nDigite o %d� Componente\n",i+1);
		
		if(i==0){
			printf("(");
		}
		if(i==1){
			printf("(%0.1f ,",v[0]);
		}
		if(i==2){
			printf("(%0.1f , %0.1f ,",v[0],v[1]);
		}
		
				scanf("%f",&v[i]);
		i++;
		}while(i<TV);
		opcao1=75;
		i=0;
		while(opcao1!='1'&&opcao1!='2'){
		system("cls");
		printf("\n\n\t\t\tComponentes= (%0.1f , %0.1f ,%0.1f)\n\n",v[0],v[1],v[2]);
		printf("\n\t\t\t Escolha a Op��o Desejada: \n");
		printf("\n\n--Pressione o bot�o [1] Para Prosseguir; ");
		printf("\n--Pressione o bot�o [2] Para Alterar os Componentes;\n\n");
		if(v[0]==0&&v[1]==0&&v[2]==0){
				printf("\n\tOBS:Vo�� Digitou um Vetor Nulo,Pessione [2] para alterar,ou continue\n");
		}
		if(opcao1!='1'&&opcao1!='2'&&i>0){
				printf("\n\tOp��o inv�lida,Digite Novamente a Op��o Desejada:\n");
		}
		fflush(stdin);
		opcao1=getche();
		i++;	
		}
	}
	system("cls");
	printf("\n\nComponentes= (%0.1f , %0.1f ,%0.1f)\n\n",v[0],v[1],v[2]);
}

float func_escalar(float v[TV],float v1[TV]){
	float e;
	e=(v[0]*v1[0]+v[1]*v1[1]+v[2]*v1[2]);
	return e;
}

void func_vetdiretor(float p[TV],float p1[TV],float vd[TV]){
	int i=0;
	for(i=0;i<3;i++)
		vd[i]=p1[i]-p[i];
}

void func_vetnormal(float v[TV],float v1[TV],float vn[TV]){
	float m[3][3];
	int i;
	for(i=0;i<3;i++){
		m[0][i]=v[i];
		m[1][i]=v1[i];
	}
	//////////////Produto Vetorial/////////////
	vn[0]=+(m[0][1]*m[1][2])-(m[0][2]*m[1][1]);
	vn[1]=+(m[0][2]*m[1][0])-(m[1][2]*m[0][0]);
	vn[2]=+(m[0][0]*m[1][1])-(m[1][0]*m[0][1]);
}
float func_vetdir(float vn[TV]){/////
		float p[TV],p1[TV];
		printf("\n Digite os Valores dos Pontos :\n");
		printf("\n Digite os valores do ponto 1\n");
			func_digitavetor(p);
		printf("\n Digite os valores do ponto 2\n");
			func_digitavetor(p1);
		func_vetdiretor(p,p1,vn);
}

float func_vetnor(float vn[TV]){/////
		float p[TV],p1[TV];
		printf("\n Digite os Valores dos vetores diretores do plano :\n");
		printf("\n Digite os valores do vetor diretor 1\n");
			func_digitavetor(p);
		printf("\n Digite os valores do vetor diretor 2\n");
			func_digitavetor(p1);
		func_vetnormal(p,p1,vn);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////MENUS///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
char menu_main(void)
{
	int i=0;
	char o=0;
	while(o!='1'&&o!='2'&&o!='3'&&o!=27){
		system("cls");
		printf("\t\tPressione o n�mero da Op��o Escolhida\n\n");
		printf("\t\t\t[ESC] Para Sair\n");
		printf("\t\t\t[1] - Posi��o Relativa\n");
		printf("\t\t\t[2] - �ngulo\n");
		printf("\t\t\t[3] - Dist�ncia\n"); 
		if(o!='1'&&o!='2'&&o!='3'&&o!=27&&i>0){
			printf("\n\tOp��o inv�lida,Digite Novamente a Op��o Desejada:\n");
		}
		o=getche();
		i++; 
	}
	system("cls");
	return o;
}
char menu_rp(void)
{
	int i=0;
	char o=0;
	while(o!='1'&&o!='2'&&o!='3'&&o!=27){
		system("cls");
		printf("\t\tPressione o n�mero da Op��o Escolhida\n\n");
		printf("\t\t\t[ESC] Para Sair\n");
		printf("\t\t\t[1] - Entre 2 Retas\n");
		printf("\t\t\t[2] - Entre 1 Reta e 1 Plano\n");
		printf("\t\t\t[3] - Entre 2 Planos\n");
		if(o!='1'&&o!='2'&&o!='3'&&o!=27&&i>0){
			printf("\n\tOp��o inv�lida,Digite Novamente a Op��o Desejada:\n");
		}
		o=getche();
		i++; 
	}
	system("cls");
	return o;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////iNTEGRA��O DE MENUS/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void executa_geral(void)
{
	char o, o2;
	float v1[3], v2[3];
	do{
		fflush(stdin);
		o=menu_main();
		switch(o)
		{
			case'1':
				fflush(stdin);
				o2=menu_rp();
				switch(o2)
				{
					case '1':
							float v1[3], p1[3], v2[3], p2[3];
								func_levetoreponto(v1, p1, v2, p2);
								main_posrelativa_rr(v1, p1, v2, p2);//posi��o relativa entre retas
					break;
					
					case '2':
							float v[3], pv[3], N[TN];
								func_reqgeraldoplano(N);
								func_levet(v, pv);
								main_posrelativa_rp(v, pv, N);
					break;
					
					case '3':	
							float N1[TN], N2[TN];
							system("cls");
								printf("\t\t1�Plano\n\n");
									func_reqgeraldoplano(N1);
								printf("\t\t2�Plano\n\n");
									func_reqgeraldoplano(N2);
								main_posrelativa_pp(N1, N2);
					break;
					
					case 27:
						system("cls");
					break;
				}
			break;
			
			case'2':
				fflush(stdin);
				o2=menu_rp();
				switch(o2)
				{
					case'1':
						main_angulo_rr();
					break;
					
					case'2':
						main_angulo_rp();
					break;
					
					case'3':
						main_angulo_pp();
					break;
					case 27:
						system("cls");
					break;
				}
			break;
		
		case'3':
			fflush(stdin);
			o2=menu_rp();
			switch(o2)
			{	
				case'1':
					float v1[3], p1[3], v2[3], p2[3], escalar;
					func_levetoreponto(v1, p1, v2, p2);
					main_distancia_rr(v1,p1,v2,p2);
				break;
				
				case'2':
					main_distancia_rp();	
				break;
				
				case'3':
					main_distancia_pp();	
				break;
				
				case 27:
					system("cls");
				break;
			}
		
			case 27:
				system("cls");
			break;
		
		}
		
	}while(o!=27);
}
int main(){
	setlocale(LC_ALL,"portuguese");
	executa_geral();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////POSI��O RELATIVA////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
void main_posrelativa_rr(float a[3], float pa[3], float b[3], float pb[3])
{
	float ab[3];
	if (func_dl(a, b) == 1)
	{
		if(func_pontonareta(pb, a, pa)==1)
			printf("As Retas R e S S�o Iguais!!!\n\n");
		else
			printf("As Retas R e S S�o Paralelas!!!\n\n");
	}
	else
	{
		func_vetdiretor(pa,pb, ab);
		if(func_ProdMisto(a, b, ab)==0)
		{
			printf("As Retas R e S S�o Concorrentes!!!\n\n");
		}
		else
			printf("As Retas R e S S�o Reversas!!!\n\n");
	}
	func_sair();
}
void main_posrelativa_rp(float v[3], float pv[3], float N[TN])
{
	if(func_ProdEScalar(v, N)!=0)
		printf("A reta � Transversal ao Plano\n\n");
	else
		if(func_pontonaEqGeral(pv, N)==1)
			printf("A Reta Esta Contida no Plano\n\n");
		else
			printf("A reta � Paralela ao Plano\n\n");
	func_sair();
}
void main_posrelativa_pp(float N1[TN], float N2[TN])
{
	float v3[TN];
	int d;
	d=func_LDouLIplan(N1, N2);
	if(d==45)
		printf("O 1�Plano � Igual ao 2�Plano\n\n");
	if(d==47)
		printf("O 1�Plano � Paralelo ao 2�Plano\n\n");
	if(d==49)
		printf("O 1�Plano � Transversal ao 2�Plano\n\n");
	func_sair();
}
int main_aut_posrelativa_rr(float a[3], float pa[3], float b[3], float pb[3])
{
	float ab[3];
	if (func_dl(a, b) == 1)
	{
		if(func_pontonareta(pb, a, pa)==1)
			return 61;
		else
			return 65;
	}
	else
	{
		func_vetdiretor(pa,pb, ab);
		if(func_ProdMisto(a, b, ab)==0)
		{
			return 67;
		}
		else
			return 69;
	}
}
int main_aut_posrelativa_rp(float v[3], float pv[3], float N[TN])
{
	if(func_ProdEScalar(v, N)!=0)
		return 55;
	else
		if(func_pontonaEqGeral(pv, N)==1)
			return 57;
		else
			return 59;
}
int main_aut_posrelativa_pp(float N1[TN], float N2[TN])
{
	float v3[TN];
	v3[0]=divisao(N1[0], N2[0]);
	v3[1]=divisao(N1[1], N2[1]);
	v3[2]=divisao(N1[2], N2[2]);
	v3[3]=divisao(N1[3], N2[3]);
	if(v3[0]==v3[1]&&v3[1]==v3[2]&&v3[2]==v3[3])
		return 45;
	else if(v3[0]==v3[1]&&v3[1]==v3[2]&&v3[2]!=v3[3])
		return 47;
	else if(v3[0]!=v3[1] || v3[1]!=v3[2] || v3[2]!=v3[1])
		return 49;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////DIST�NCIA////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
void main_distancia_rr(float v1[3], float p1[3], float v2[3], float p2[3])
{
	int i=0;
	float v1Xv2[TV],ab[TV],v3[TV],escalar;
	i=main_aut_posrelativa_rr(v1,p1,v2,p2);
	if(i==61||i==65){
 	  		main_aut_distancia_por(p2,p1,v2);
	  	}
	else
 	  	if(i==69){
		 	func_Vetorial(v1, v2, v1Xv2);
		 	main_aut_distancia_pop(p1,p2,v1Xv2);
		}
		else
	   	  	if(i==67){
	   	  		printf("\n\tDIST�NCIA= 0\n");
			}
	func_sair();
}
void main_distancia_rp(void){
	float v[TV],v1[TV],p[TV],p1[TV],vn[TN],vd[TV],vd1[TV],vn1[TV];
	int i=0,j=0,z=0;
	printf("\nDigite um ponto do plano\n");
		func_digitaponto(p1);
	printf("\nDigite a equa��o geral do plano\n");
		func_reqgeraldoplano(vn);
	printf("\nDigite um ponto da reta\n");
		func_digitaponto(p);
	printf("\nDigite o vetor diretor da reta\n");
		func_digitavetor(vd);
	i=main_aut_posrelativa_rp(vd,p,vn);
	system("cls");
	printf("\n Dist�ncia entre a reta= (%0.1f,%0.1f,%0.1f) ",vd[0],vd[1],vd[2]);
		printf(" e o plano= "); 
		for(z=0;z<4;z++){
			if(vn[z]>=0)
				printf("+%0.1f",vn[z]);
			else
			 	printf("%0.1f",vn[z]);	
		}
		printf(" �:\n"); 
	if(i==55||i==57){
		printf("\n\tDIST�NCIA= 0\n");
	}
	else
	  if(i==59){
	  	for(j=0;j<TV;j++){
	  		vn1[j]=vn[j];
		}
	  	main_aut_distancia_pop(p,p1,vn1);
	  }
	func_sair();
}
void main_distancia_pp(void){
	float p[TV],p1[TV],vn[TV],va[TN],va1[TN],vn1[TV];
	int i=0,j=0,z=0;
	printf("\nDigite um ponto do primeiro plano\n");
		func_digitaponto(p);
	printf("\nDigite a equa��o geral do plano\n");
		func_reqgeraldoplano(va);
	printf("\nDigite um ponto do outro plano\n");
		func_digitaponto(p1);
	printf("\nDigite a equa��o geral do outro plano\n");
		func_reqgeraldoplano(va1);
	i=main_aut_posrelativa_pp(va,va1);
	for(j=0;j<TV;j++){
	  	vn[j]=va[j];
	  	vn1[j]=va1[j];
	}
	system("cls");
		printf("\nDist�ncia entre o plano 1= ");
		for(z=0;z<4;z++){
			if(va[z]>=0)
				printf("+%0.1f",va[z]);
			else
			 	printf("%0.1f",va[z]);	
		}
		printf(" e o plano 2= "); 
		for(z=0;z<4;z++){
			if(va1[z]>=0)
				printf("+%0.1f",va1[z]);
			else
			 	printf("%0.1f",va1[z]);	
		}
		printf(" �:\n"); 
	if(i==45||i==49){//problemas
		printf("\n\tDIST�NCIA= 0\n");
	}
	else
	  if(i==47){
	  	main_aut_distancia_pop(p,p1,vn1);
	  }
	func_sair();
}
void main_aut_distancia_por(float p[TV],float p1[TV],float vd1[TV]){
	float vdp[TV],vd2[TV],aux,aux1;
		func_vetdiretor(p1,p,vdp);//talves de erro
		func_vetnormal(vdp,vd1,vd2);//produto vetorial
		aux=func_modulo(vd2);
		if(aux<0){
				aux=aux*(-1);
		}	
		aux1=func_modulo(vd1);
		if(aux1<0){
				aux1=aux1*(-1);
		}	
		printf("\n A DISTANCIA � %f\n",aux/aux1);
}
void main_aut_distancia_pop(float p[TV], float p1[TV],float vn[TV]){                             
	float aux=0,vd1[TV];
	    	func_vetdiretor(p1,p,vd1);
	    	aux=func_escalar(vd1,vn);
	    	if(aux<0){
				aux=aux*(-1);
			}	
			printf("\n A DISTANCIA � %f\n",aux/func_modulo(vn));	
}
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////�NGULOS/////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
void main_angulo_rr(void){
	int opcao;
	float v[TV],v1[TV],aux=0;
			printf("\n Forne�a os vetores diretores\n");
			printf("\n Digite os valores do vetor 1\n");
				func_digitavetor(v);
			printf("\n Digite os valores do vetor 2\n");
				func_digitavetor(v1);
			aux=func_escalar(v,v1)/(func_modulo(v)*func_modulo(v1));
		  	if(aux<0){ 
		  		aux=aux*(-1);
		  	}
		  	system("cls");
		  	printf("\n\t�ngulo entre a Reta 1=(%0.1f,%0.1f,%0.1f) e a Reta 2=(%0.1f,%0.1f,%0.1f) �:\n",v[0],v[1],v[2],v1[0],v1[1],v1[2]);
			printf("\n\n O= COS-� %f RAD",aux);
 		func_sair();
}
void main_angulo_rp(void){
	int opcao;
	float v[TV],v1[TV],aux=0;
			printf("\n Forne�a o vetor diretor\n");
			printf("\n Digite os valores do vetor diretor da reta\n");
				func_digitavetor(v);
			printf("\n Forne�a o vetor normal\n");
			printf("\n Digite os valores do vetor normal ao plano\n");
				func_digitavetor(v1);
			aux=func_escalar(v,v1)/(func_modulo(v)*func_modulo(v1));
		  	if(aux<0){ 
		  		aux=aux*(-1);
		  	}
		  	system("cls");
		  	printf("\n\t�ngulo entre a Reta =(%0.1f,%0.1f,%0.1f) e o Plano =(%0.1f,%0.1f,%0.1f) �:\n",v[0],v[1],v[2],v1[0],v1[1],v1[2]);
			printf("\n\n O= SIN-� %f ",aux);
		func_sair();	
}
void main_angulo_pp(void){
	int opcao;
	float v[TV],v1[TV],aux=0;
			printf("\n Forne�a os vetores normais\n");
			printf("\n Digite os valores do vetor do plano 1\n");
				func_digitavetor(v);
			printf("\n Digite os valores do vetor do plano 2\n");
				func_digitavetor(v1);
			aux=func_escalar(v,v1)/(func_modulo(v)*func_modulo(v1));
		  	if(aux<0){ 
		  		aux=aux*(-1);
		  	}
		  	system("cls");
		  	printf("\n\t�ngulo entre o plano 1=(%0.1f,%0.1f,%0.1f) e o plano 2=(%0.1f,%0.1f,%0.1f) �:\n",v[0],v[1],v[2],v1[0],v1[1],v1[2]);
			printf("\n\n\t\t\t O= COS-� %f rad",aux);
		func_sair();	
}
